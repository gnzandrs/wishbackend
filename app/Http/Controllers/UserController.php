<?php namespace App\Http\Controllers;

use App\Models\Entities\User;
use App\Models\Entities\Country;
use App\Models\Entities\City;
use App\Models\Entities\WishList;
use App\Models\Managers\RegisterManager;
use App\Models\Managers\UserManager;
use App\Models\Repositories\UserRepo;
use App\Models\Repositories\ConfigurationRepo;
use App\Models\Repositories\WishListRepo;
use App\Models\Entities\Log;
use App\Models\Repositories\LogRepo;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;


class UserController extends Controller {

	protected $userRepo;
    protected $configRepo;
    protected $wishlistRepo;
    protected $logRepo;

    public function __construct(UserRepo $userRepo, ConfigurationRepo $configRepo,
                                WishListRepo $wishlistRepo, LogRepo $logRepo)
	{
        $this->configRepo = $configRepo;
        $this->userRepo = $userRepo;
        $this->wishlistRepo = $wishlistRepo;
        $this->logRepo = $logRepo;
	}

    // check availability of username
    public function check($username)
    {
        try{
            \Log::info('UserController check($username)');
            $available = $this->userRepo->userCheck($username);
            return $available;
        }
        catch (Exception $e)
        {
            \Log::error('UserController check($username): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'catch', $e);
            return 0;
        }
    }

    // check availability of email
    public function checkEmail($email)
    {
        try{
            \Log::info('UserController checkEmail($email)');
            $available = $this->userRepo->emailCheck($email);
            return $available;
        }
        catch (Exception $e)
        {
            \Log::error('UserController emailCheck($email): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'catch', $e);
            return 0;
        }
    }

    // return cities list
    public function citiesList()
    {
        try{
            //\Log::info('UserController citiesList()');
            $countryCode = Input::get('code');
            $cities = \DB::table('city')
                ->join('country', 'city.country_id', '=', 'country.id')
                ->where('country.id', $countryCode)
                ->select('city.id', 'city.code', 'city.name')
                ->get();

						return response()->json($cities);
        }
        catch (Exception $e)
        {
            \Log::error('UserController citiesList(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'catch', $e);
            return 0;
        }
    }

		// return countries list
		public function countriesList()
		{
			try {
				$countrys = Country::all();
				return response()->json($countrys);
			}
			catch (Exception $e)
			{
				Log::error('UserController countriesList(): '.$e);
				$this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
				return 0;
			}
		}

    // get user avatar by id
    public function getAvatarImage()
    {
        try{
            \Log::info('UserController getAvatarImage()');
            $userId = Auth::user()->id;
            $image = $this->userRepo->getAvatarImage($userId);
            if (Request::ajax())
            {
                return $image;
            }
        }
        catch (Exception $e)
        {
            \Log::error('UserController getAvatarImage(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'catch', $e);
            return 'error';
        }
    }

    // record a new user
    public function register(Request $request)
    {
        try {
						\Log::info('UserController register()');

            $user = $this->userRepo->newUser();
            $manager = new RegisterManager($user, $request->input('user'));
            $result = $manager->save();

						if ($result)
						{
								$result = $this->userRepo->createDirectoryTree($user);
						} else {
								return response()->json($user->errors);
						}

						return response()->json($user->username);
        }
        catch (Exception $e)
        {
            \Log::error('UserController register(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'catch', $e);
            return response()->json(0);
        }
    }

    // return user object with images and wishlists
    public function show($id)
    {
        try{
            $user = $this->userRepo->find($id);
            $user->UserImage;
            $user->lists;
            return $user->toJson();
        }
        catch (Exception $e)
        {
            Log::error('UserController show($id): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // sign-up view
    public function signUp()
    {
        try{
            $countrys = Country::all();
            return $countrys->toJson();
        }
        catch (Exception $e)
        {
            Log::error('UserController signUp(): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }

    // public view of the lists
    public function wishListShow($id)
    {
        try{
            $wishlist = $this->wishlistRepo->find($id);
            $wishlist->Wishs;
            $wishlist->User;
            return $wishlist->toJson();
        }
        catch (Exception $e)
        {
            Log::error('UserController wishListShow($id): '.$e);
            $this->logRepo->newLog('UserController.php', 'UserController.php', 'error catch', $e);
            return 0;
        }
    }
}
