<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// User
Route::get('user/sign-up', ['as' => 'user/sign-up', 'uses' => 'UserController@signUp']);
Route::get('user/check/{username}', ['as' => 'user/check', 'uses' => 'UserController@check']);
Route::get('user/checkEmail/{email}', ['as' => 'user/checkEmail', 'uses' => 'UserController@checkEmail']);
Route::get('user/countrieslist', ['as' => 'user/countriesList', 'uses' => 'UserController@countriesList']);
Route::post('user/login', ['as' => 'user/login', 'uses' => 'Auth\LoginController@authenticate']);                                                                                     
Route::get('user/show/{id}', ['as' => 'user/show', 'uses' => 'UserController@show']);
Route::get('user/wishlist/{id}', ['as' => 'user/wishlist', 'uses' => 'UserController@wishListShow']);
Route::post('user/register', ['as' => 'register', 'uses' => 'UserController@register']);
Route::post('user/citiesList', ['as' => 'user/citiesList', 'uses' => 'UserController@citiesList']);
Route::post('user/avatarImage', ['as' => 'user/avatarImage', 'uses' => 'UserController@getAvatarImage']);


Route::group(['middleware' => 'auth.basic'], function () {

    Route::get('category/list', ['as' => 'category', 'uses' => 'CategoryController@getCategories']);
    Route::get('category/{name}', ['as' => 'category', 'uses' => 'CategoryController@show']);
    Route::get('category/search/{category}/{search}', ['as' => 'category/search', 'uses' => 'CategoryController@searchByCategory']);

});


Route::get('/', function () {
    return 'welcome to laravel 5 backend api :)';
});
