<?php namespace App\Models\Entities;

use Whoops\Example\Exception;

class City extends \Eloquent {
    protected $table = 'city';
    protected $fillable = ['name','code'];

    public function country()
    {
        return $this->belongsTo('App\Models\Entities\Country', 'country_id','id');
    }
}
