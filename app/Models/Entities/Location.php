<?php namespace App\Models\Entities;

class Location extends \Eloquent {
    protected $table = 'location';
    protected $fillable = ['name','latitude','longitude'];

    public function wishs()
    {
        return $this->hasMany('App\Models\Entities\Wish', 'location_id', 'id');
    }
}
