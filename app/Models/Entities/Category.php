<?php namespace App\Models\Entities;

class Category extends \Eloquent {
    protected $table = 'category';
    protected $fillable = ['name'];

    public function wishs()
    {
        return $this->hasMany('App\Models\Entities\Wish', 'category_id', 'id');
    }
}
