<?php namespace App\Models\Entities;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('username', 'name', 'lastname', 'email', 'genre', 'city_id', 'password');
    public $errors;

	public function configuration()
    {
        return $this->hasOne('App\Models\Entities\Configuration', 'user_id', 'id');
    }

    public function city()
    {
        return $this->hasOne('App\Models\Entities\City', 'id', 'city_id');
    }

    public function setPasswordAttribute($value)
    {
        if (!empty($value))
        {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function lists()
    {
    	return $this->hasMany('App\Models\Entities\WishList');
    }

    public function UserImage()
    {
        return $this->hasOne('App\Models\Entities\UserImage', 'user_id', 'id');
    }

}
