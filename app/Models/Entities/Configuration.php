<?php namespace App\Models\Entities;

class Configuration extends \Eloquent {

    protected $table = 'configuration';
    protected $fillable = array('notificacion', 'deal');

	public function user()
	{
		return $this->belongsTo('App\Models\Entities\User', 'user_id', 'id');
	}
}